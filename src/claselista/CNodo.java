
package claselista;

public class CNodo {
    private CNodo Siguiente;  //siguiente el CNodo que sigue
    private String Contiene;  // indica lo que hay dentro del CNodo

    // constructor que no contiene nada
   public CNodo (){
       setSiguiente(null);
       setContiene(null);
   }
   
   // Constructor recibe información 
    public CNodo (String Cont){
       setSiguiente(null);
       setContiene(Cont);
   }
    // Constructor recibe CNodo 
    public CNodo (String Cont, CNodo Nodo){
       setSiguiente(Nodo);
       setContiene(Cont);
   }
    public CNodo getSiguiente() {
        return Siguiente;
    }

   
    public void setSiguiente(CNodo Siguiente) {
        this.Siguiente = Siguiente;
    }

   
    public String getContiene() {
        return Contiene;
    }

   
    public void setContiene(String Contiene) {
        this.Contiene = Contiene;
    }
    
}
